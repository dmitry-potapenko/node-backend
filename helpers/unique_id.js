module.exports = (kwargs) => {

  return { uniqueId };

  function uniqueId() {
    return Math.floor(Math.random() * Date.now()).toString(16).slice(0, 8);
  }
};