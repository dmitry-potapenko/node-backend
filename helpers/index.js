const glob = require('glob');
const path = require('path');

module.exports = (kwargs) => {
  var files = glob.sync(__dirname + '/!(*index).js');
  var result = {};
  var m = null;
  files.forEach(function (p) {
    m = require(p);
    if (m && typeof m == 'function') {
      m = m(kwargs);
    }
    result = Object.assign(result, m);
  });
  return result;
};