const path = require('path');

module.exports.init = init;

function init(kwargs) {
  const { root } = kwargs;
  kwargs.helpers = require(path.join(root, '/helpers'))(kwargs);
  // init components
  require(path.join(root, '/endpoints'))(kwargs);
}