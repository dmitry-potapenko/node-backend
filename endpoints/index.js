const glob = require('glob');

module.exports = function(kwargs) {
  //load components
  const endpoints = glob.sync(`${__dirname}/!(*.js)`);
  endpoints.forEach(function(ep) {
    require(ep)(kwargs);
  });
};