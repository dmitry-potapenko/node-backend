const
  glob = require('glob'),
  path = require('path');


module.exports = function(kwargs) {
  // load entities
  const { app } = kwargs;
  const entities = glob.sync(`${__dirname}/entities/!(*.js)`);
  const endpoint = __dirname.split('/').pop();

  entities.forEach(function(en) {
    const entity = en.split('/').pop();
    const initRoutes = require(path.join(en, `/${entity}.routes`));
    app.use(
      `/${endpoint}/${entity}`,
      initRoutes(Object.assign({ entity }, kwargs))
    );
  });
};