const express = require('express');

module.exports = (kwargs) => {
  const router = express.Router();
  const { entity } = kwargs;
  const controllers = require(`./${entity}.controllers`)(kwargs);
  router.get('/', controllers.index);
  return router;
};